package ru.nlmk.jse29.service;

public class FileReaderService {
    private final FileReader fileReader;

    public FileReaderService(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    public String read(String address) {
        StringBuilder result = new StringBuilder();
        fileReader.open(address);
        String line = "";
        while (line != null) {
            result.append(line.toUpperCase());
            line = fileReader.next();
        }
        return result.toString();
    }
}
