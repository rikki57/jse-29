package ru.nlmk.jse29.service;

public interface FileReader extends AutoCloseable {
    boolean open(String address);

    String next();

    void close();
}
