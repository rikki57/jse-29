package ru.nlmk.jse29.service;

import org.apache.commons.lang3.StringUtils;
import ru.nlmk.jse29.model.User;
import ru.nlmk.jse29.repository.MyRepository;

import java.util.ArrayList;
import java.util.List;

public class MyService {
    private MyRepository myRepository;

    public MyService(MyRepository myRepository) {
        this.myRepository = myRepository;
    }

    public Integer sum(Integer arg1, Integer arg2) {
        if (arg1 < 1 || arg2 < 1) {
            throw new IllegalArgumentException();
        }
        return arg1 + arg2;
    }

    public User createUser(String name, String city) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(city)) {
            throw new IllegalArgumentException();
        }
        User user = new User(name, city);
        boolean result = myRepository.create(user);
        if (result) {
            return user;
        } else {
            return null;
        }
    }

    public List<String> getUserNameParts(String name) {
        List<String> result = new ArrayList<>();
        name = name.trim();
        Integer nextPoint = name.indexOf(" ");
        while (nextPoint != -1) {
            result.add(name.substring(0, nextPoint));
            name = name.substring(nextPoint + 1);
            nextPoint = name.indexOf(" ");
        }
        result.add(name);
        return result;
    }

    public String readResource(String address) {
        try (FileReader fileReader = new FileReaderImpl()) {
            return new FileReaderService(fileReader).read(address);
        } catch (Exception e) {
            return "";
        }
    }
}
