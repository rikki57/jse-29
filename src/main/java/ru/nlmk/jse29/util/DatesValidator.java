package ru.nlmk.jse29.util;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class DatesValidator {
    //InstantConverter.toInstantTruncatedToDays("2020-08-02T12:00:00.000Z")
    public static boolean isDateInRangeExcludeNulls(Instant fromS, Instant toS, Instant comparedDateTime) {
        if ((fromS == null) && (toS == null)) {
            return true;
        } else if (comparedDateTime == null) {
            return false;
        }
        Instant from = InstantConverter.truncateToDays(fromS);
        Instant to = InstantConverter.truncateToDays(toS);
        Instant truncComparedDate = comparedDateTime.truncatedTo(ChronoUnit.DAYS);
        if (from != null && to != null) {
            return (truncComparedDate.isAfter(from) || truncComparedDate.equals(from)) &&
                    (truncComparedDate.isBefore(to) || truncComparedDate.equals(to));
        } else {
            return from != null && (truncComparedDate.isAfter(from) || truncComparedDate.equals(from)) ||
                    to != null && (truncComparedDate.isBefore(to) || truncComparedDate.equals(to));
        }
    }
}
