package ru.nlmk.jse29.repository;

import ru.nlmk.jse29.model.User;

import java.io.IOException;

public class MyRepository {
    public boolean create(User user) {
        try {
            //impl
            if (5 > 10) {
                throw new IOException();
            }
        } catch (IOException e) {
            //обработка исключения
            return false;
        }
        return true;
    }

    public User read(int id) {
        //impl
        return new User(1L, "Alex", "Moscow");
    }

    public User update(int id, User user) {
        //impl
        return user;
    }

    public boolean delete(Long id) {
        //impl
        return true;
    }
}
