package ru.nlmk.jse29.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class InstantConverterTest {
    @Test
    void fromInstantNull() {
        assertNull(InstantConverter.fromInstant(null));
    }

    @Test
    void fromInstantNotNull() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String expected = dateFormat.format(date);
        assertEquals(expected, InstantConverter.fromInstant(InstantConverter.truncateToDays(Instant.now())).substring(0, 10));
    }

    @Test
    void toInstant() {
    }

    @Test
    void toInstantTruncatedToDays() {
    }

    @Test
    void truncateToDays() {
    }
}