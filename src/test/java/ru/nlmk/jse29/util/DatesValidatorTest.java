package ru.nlmk.jse29.util;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DatesValidatorTest {
    @Test
    void isDateInRangeExcludeNullsNullPeriod() {
        assertTrue(DatesValidator.isDateInRangeExcludeNulls(null, null, Instant.now()));
    }

    @Test
    void isDateInRangeExcludeNullsNullCheckedDate() {
        assertFalse(DatesValidator.isDateInRangeExcludeNulls(Instant.now(), Instant.now(), null));
    }

    @Test
    void isDateInRangeExcludeNullsNormal() {
        assertTrue(DatesValidator.isDateInRangeExcludeNulls(
                Instant.now(),
                Instant.now().plus(4, ChronoUnit.DAYS),
                Instant.now().plus(2, ChronoUnit.DAYS)));
    }

    @Test
    void isDateInRangeExcludeNullsNormal2() {
        assertFalse(DatesValidator.isDateInRangeExcludeNulls(
                Instant.now(),
                Instant.now().plus(4, ChronoUnit.DAYS),
                Instant.now().plus(6, ChronoUnit.DAYS)));
    }

    @Test
    void isDateInRangeExcludeNullsNormal3() {
        assertTrue(DatesValidator.isDateInRangeExcludeNulls(
                Instant.now(),
                null,
                Instant.now().plus(6, ChronoUnit.DAYS)));
    }

    @Test
    void isDateInRangeExcludeNullsNormal4() {
        assertFalse(DatesValidator.isDateInRangeExcludeNulls(
                Instant.now(),
                null,
                Instant.now().minus(6, ChronoUnit.DAYS)));
    }
}