package ru.nlmk.jse29.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import ru.nlmk.jse29.model.User;
import ru.nlmk.jse29.repository.MyRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MyServiceTest {
    private static final String NAME = "name";
    private static final String CITY = "city";

    MyService myService;
    MyRepository myMockRepository = mock(MyRepository.class);

    @BeforeEach
    private void setup() {
        myService = new MyService(myMockRepository);
    }

    @Test
    void sumCorrect() {
        assertEquals(4, myService.sum(1, 3));
    }

    @Test
    void sumException() {
        assertThrows(IllegalArgumentException.class, () -> myService.sum(0, 1));
    }

    @Test
    void createUserNormal() {
        User expected = new User(NAME, CITY);
        when(myMockRepository.create(any())).thenReturn(true);
        User result = myService.createUser(NAME, CITY);
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void createUserException() {
        assertThrows(IllegalArgumentException.class, () -> myService.createUser("", null));
    }

    @Test
    void createUserRepositoryError() {
        when(myMockRepository.create(any())).thenReturn(false);
        User result = myService.createUser(NAME, CITY);
        assertNull(result);
    }

    @Test
    void testNormal() {
        List<String> result = myService.getUserNameParts("Иванов Иван Петрович");
        assertEquals("Иванов", result.get(0));
        assertEquals("Иван", result.get(1));
        assertEquals("Петрович", result.get(2));
    }

    @Test
    void testNormalShort() {
        List<String> result = myService.getUserNameParts("Иванов Иван");
        assertEquals("Иванов", result.get(0));
        assertEquals("Иван", result.get(1));
    }

    @Test
    void testNormalShortTrim() {
        List<String> result = myService.getUserNameParts("  Иванов Иван Петрович ");
        assertEquals("Иванов", result.get(0));
        assertEquals("Иван", result.get(1));
        assertEquals("Петрович", result.get(2));
    }

    @Test
    void readResource() {

    }
}