package ru.nlmk.jse29.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FileReaderServiceTest {
    private static final String STRING_1 = "String 1 \n";
    private static final String STRING_2 = "String 2 \n";
    private static final String STRING_3 = "String 3 \n";
    private static final String FILENAME = "filename";

    private final FileReader fileReader = Mockito.mock(FileReader.class);
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private FileReaderService fileReaderService;

    @BeforeEach
    void setUp() {
        fileReaderService = new FileReaderService(fileReader);
    }

    @Test
    void read() {
        when(fileReader.next()).thenReturn(STRING_1).thenReturn(STRING_2).thenReturn(STRING_3).thenReturn(null);
        String result = fileReaderService.read(FILENAME);
        verify(fileReader, times(1)).open(FILENAME);
        assertEquals(STRING_1 + STRING_2 + STRING_3, result);
    }

    @Test
    void readVar2() {
        ArgumentCaptor<String> adressCaptor = ArgumentCaptor.forClass(String.class);
        when(fileReader.next()).thenReturn(STRING_1).thenReturn(STRING_2).thenReturn(STRING_3).thenReturn(null);
        String result = fileReaderService.read(FILENAME);
        verify(fileReader, times(1)).open(adressCaptor.capture());
        assertEquals(STRING_1 + STRING_2 + STRING_3, result);
        assertEquals(FILENAME, adressCaptor.getValue());
    }
}